let React = require('react');
let ReactDOM = require('react-dom');
let styled = require("styled-components").default;
let media = require("styled-media-query").default;
let Database = require("database-api").default;

let config = require("visual-config-exposer").default;

let Leaderboard = require("./postgame").leaderboard;

let database = new Database();

const Button = styled.button`
    width: 60%;
    height: 60px;
    border-radius: 20px;
    font-size: 1.5rem;
    background-color: ${config.preGameScreen.buttonColor};
    color: ${config.preGameScreen.buttonTextColor};
    border: none;
    outline: none;
    ${props => props.extra}
     @media (max-width: 370px) {
        box-sizing: border-box;
        font-size: 6vw;
        padding: 16px;

    }
`;

let soundButton;

class SoundButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            src: config.preGameScreen.soundEnabledIcon
        };
    }

    render() {
        return React.createElement("img", {
            src: this.state.src,
            onClick: () => {
                window.soundEnabled = !window.soundEnabled;
                this.setState({src: window.soundEnabled ? config.preGameScreen.soundEnabledIcon : config.preGameScreen.soundDisabledIcon});
            },
            id: "button",
            style: {
                maxWidth: "40px",
                maxHeight: "40px",
                position: "absolute",
                bottom: 0,
                right: 0,
                marginRight: "15px",
                marginBottom: "15px",
                objectFit: "contain"
            }
        });
    }
}

const TitleText = styled.h1`
    font-size: ${config.preGameScreen.titleTextSize}px;
    margin-bottom: 20px;
    @media (max-width: 370px) {
        box-sizing: border-box;
        font-size: 8vw;
    } 
`;

const TitleImage = styled.img`
    display: block;
    margin-left: auto;
    margin-right: auto;
    object-fit: contain;
    width: ${config.preGameScreen.titleImageSize}px;
    height: ${config.preGameScreen.ttileImageSize}px;
    @media (max-width: 320px) {
        width: 50%;
    }
`;

const cardHeight = 450;
const cardWidth = window.mobile() ? 280 : 350;

const Card = styled.div`
    background-color: ${config.preGameScreen.cardColor};
    width: ${cardWidth}px;
    height: ${cardHeight}px;
    border-radius: 30px;
    text-align: center;
    margin: auto;
    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.75);
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -o-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    box-sizing: border-box;
    ${props => props.extra}
    @media (max-width: 370px) {
        width: 95%;
        padding: 40px 10px;
        height: auto;
    }
`;

class PreGameScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showLeaderboard: false,
            leaderboardData: null
        };
    }

    render() {
        if (this.state.showLeaderboard) {
            return(
                <Card extra="padding: 10px; height:auto; @media (max-width: 370px) { width: 95%; padding: 21px 10px 0px !important; height: auto; }">
                    <Leaderboard height={`${cardHeight - 120}px`} data={this.state.leaderboardData}></Leaderboard> 
                    <Button id="button" extra="margin-bottom: 20px;margin-top: 20px;" onClick={() => {this.setState({showLeaderboard: false})}}>Back</Button>
                </Card>
            )
        }

        return (
            <Card>
                <TitleImage src={config.preGameScreen.titleImage}></TitleImage>
                <TitleText>{config.preGameScreen.titleText}</TitleText>
                <Button id="button" onClick={() => {window.setScreen("gameScreen"); window.restartGame();}}>{config.preGameScreen.playButtonText}</Button>
                {
                    config.preGameScreen.showLeaderboardButton &&
                    <Button id="button" extra="margin-top: 20px;" onClick={ () => {
                        database.getLeaderBoard().then(data => {
                            let sortedData = data.sort((a, b) => parseInt(a.score) < parseInt(b.score));
                            this.setState({
                                leaderboardData: sortedData,
                                showLeaderboard: true
                            });
                        })
                    } }>{config.preGameScreen.leaderboardButtonText}</Button>
                }
                {
                    config.preGameScreen.showSoundButton &&
                    <SoundButton/>
                }
            </Card>
        );
    }
}

module.exports = <PreGameScreen/>;

